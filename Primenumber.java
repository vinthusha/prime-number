import java.util.Scanner;

public class Primenumber {

	public static void main(String args[]) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter any number:");

		int num = scan.nextInt();

		if (num == 0 || num == 1) {
			System.out.println(num + " is not a prime number");
		} else {
			boolean isPrime = true;
			for (int i = 2; i <= num / 2; i++) {

				if (num % i == 0) {
					isPrime = false;
					break;
				}
			}

			if (isPrime)
				System.out.println(num + " is a Prime Number");
			else
				System.out.println(num + " is not a Prime Number");
		}
	}
}
